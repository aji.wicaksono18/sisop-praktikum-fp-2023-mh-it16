#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>

#define PORT 4443

struct table
{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};

struct AllowedDatabase
{
	char database[10000];
	char name[10000];
};

struct Allowed
{
	char name[10000];
	char password[10000];
};

void createUser(const char *nama, const char *password);

int cekUserExist(const char *username);

void insertPermission(const char *nama, const char *database);

int cekAllowedDatabase(const char *nama, const char *database);

int findColumn(const char *tablePath, const char *columnName);

int deleteColumn(const char *table, int index);

int updateColumn(const char *table, int index, const char *ganti);

int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where);

int deleteTableWhere(const char *table, int index, const char *kolom, const char *where);

void writelog(char *perintah, char *nama);

int deleteTable(const char *table, const char *namaTable);


int main()
{
	int serverSocket, ret;
	struct sockaddr_in serverAddress;

	int clientSocket;
	struct sockaddr_in clientAddress;

	socklen_t addressSize;

	char dataBuffer[1024];
	pid_t childProcess;

	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0)
	{
		printf("Error saat membuat koneksi.\n");
		exit(1);
	}
	printf("Socket Server berhasil dibuat.\n");

	memset(&serverAddress, '\0', sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORT);
	serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
	if (ret < 0)
	{
		printf("Gagal melakukan binding.\n");
		exit(1);
	}
	printf("Terikat pada port %d\n", 4444);

	if (listen(serverSocket, 10) == 0)
	{
		printf("Server sedang berjalan...\n");
	}
	else
	{
		printf("Gagal melakukan binding.\n");
	}

	while (1)
	{
		clientSocket = accept(serverSocket, (struct sockaddr *)&clientAddress, &addressSize);
		if (clientSocket < 0)
		{
			exit(1);
		}
		printf("Koneksi diterima dari %s:%d\n", inet_ntoa(clientAddress.sin_addr), ntohs(clientAddress.sin_port));

		if ((childProcess = fork()) == 0)
		{
			close(serverSocket);
			while (1)
			{
				recv(clientSocket, dataBuffer, 1024, 0);
				char *token;
				char dataBufferCopy[32000];
				strcpy(dataBufferCopy, dataBuffer);
				char command[100][10000];
				token = strtok(dataBufferCopy, ":");
				int i = 0;
				char currentDatabase[1000];
				while (token != NULL)
				{
					strcpy(command[i], token);
					i++;
					token = strtok(NULL, ":");
				}

                if (strcmp(perintah[0], "cUser") == 0)
				{
					if (strcmp(perintah[3], "0") == 0)
					{
						createUser(perintah[1], perintah[2]);
					}
					else
					{
						char peringatan[] = "Tidak diizinkan";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
					}
				}

                else if (strcmp(perintah[0], "cDatabase") == 0)
				{
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", perintah[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", lokasi, perintah[2], perintah[1]);
					mkdir(lokasi, 0777);
					insertPermission(perintah[2], perintah[1]);
				}

                else if (strcmp(perintah[0], "gPermission") == 0)
				{
					if (strcmp(perintah[3], "0") == 0)
					{
						int exist = cekUserExist(perintah[2]);
						if (exist == 1)
						{
							insertPermission(perintah[2], perintah[1]);
						}
						else
						{
							char peringatan[] = "User tidak ditemukan";
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
						}
					}
					else
					{
						char peringatan[] = "Anda tidak diizinkan";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
					}
				}

				else if (strcmp(perintah[0], "cekCurrentDatabase") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
					}
					send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}

				else if (strcmp(perintah[0], "uDatabase") == 0)
				{
					if (strcmp(perintah[3], "0") != 0)
					{
						int Allowed = cekAllowedDatabase(perintah[2], perintah[1]);
						if (Allowed != 1)
						{
							char peringatan[] = "Akses_database : Anda tidak diizinkan";
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
						}
						else
						{
							strncpy(currentDatabase, perintah[1], sizeof(perintah[1]));
							char peringatan[] = "Akses_database : Diizinkan";
							printf("currentDatabase = %s\n", currentDatabase);
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
						}
					}
				}

				else if (strcmp(perintah[0], "cTable") == 0)
				{
					printf("%s\n", perintah[1]);
					char *tokens;
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
					}
					else
					{
						char daftarQuery[100][10000];
						char copyPerintah[20000];
						snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
						tokens = strtok(copyPerintah, "(), ");
						int jumlah = 0;
						while (tokens != NULL)
						{
							strcpy(daftarQuery[jumlah], tokens);
							printf("%s\n", daftarQuery[jumlah]);
							jumlah++;
							tokens = strtok(NULL, "(), ");
						}
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "../database/databases/%s/%s", currentDatabase, daftarQuery[2]);
						int iterasi = 0;
						int iterasiData = 3;
						struct table kolom;
						while (jumlah > 3)
						{
							strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
							printf("%s\n", kolom.data[iterasi]);
							strcpy(kolom.type[iterasi], daftarQuery[iterasiData + 1]);
							iterasiData = iterasiData + 2;
							jumlah = jumlah - 2;
							iterasi++;
						}
						kolom.jumlahkolom = iterasi;
						printf("iterasi = %d\n", iterasi);
						FILE *fp;
						printf("%s\n", buatTable);
						fp = fopen(buatTable, "ab");
						fwrite(&kolom, sizeof(kolom), 1, fp);
						fclose(fp);
					}
				}

				else if (strcmp(perintah[0], "dTable") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char hapus[20000];
					snprintf(hapus, sizeof hapus, "databases/%s/%s", currentDatabase, perintah[1]);
					remove(hapus);
					char peringatan[] = "Table telah dihapus";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}

                else if (strcmp(perintah[0], "dDatabase") == 0)
				{
					int Allowed = cekAllowedDatabase(perintah[2], perintah[1]);
					if (Allowed != 1)
					{
						char peringatan[] = "Akses_database : Anda tidak diizinkan";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					else
					{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", perintah[1]);
						system(hapus);
						char peringatan[] = "Database telah dihapus";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
					}
				}

				else if (strcmp(perintah[0], "insert") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(), ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						jumlah++;
						tokens = strtok(NULL, "\'(), ");
					}
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[2]);
					FILE *fp;
					int banyakKolom;
					fp = fopen(buatTable, "r");
					if (fp == NULL)
					{
						char peringatan[] = "Table tidak ditemukan";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					else
					{
						struct table user;
						fread(&user, sizeof(user), 1, fp);
						banyakKolom = user.jumlahkolom;
						fclose(fp);
					}
					int iterasi = 0;
					int iterasiData = 3;
					struct table kolom;
					while (jumlah > 3)
					{
						strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
						printf("%s\n", kolom.data[iterasi]);
						strcpy(kolom.type[iterasi], "string");
						iterasiData++;
						jumlah = jumlah - 1;
						iterasi++;
					}
					kolom.jumlahkolom = iterasi;
					if (banyakKolom != kolom.jumlahkolom)
					{
						char peringatan[] = "Input Anda tidak cocok dengan kolom";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					printf("iterasi = %d\n", iterasi);
					FILE *fp1;
					printf("%s\n", buatTable);
					fp1 = fopen(buatTable, "ab");
					fwrite(&kolom, sizeof(kolom), 1, fp1);
					fclose(fp1);
					char peringatan[] = "Data telah dimasukkan";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}

				else if (strcmp(perintah[0], "dColumn") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, perintah[2]);
					int index = findColumn(buatTable, perintah[1]);
					if (index == -1)
					{
						char peringatan[] = "Kolom tidak ditemukan";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					deleteColumn(buatTable, index);
					char peringatan[] = "Kolom telah dihapus";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}

				else if (strcmp(perintah[0], "delete") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("jumlah = %d\n", jumlah);
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[2]);
					if (jumlah == 3)
					{
						deleteTable(buatTable, daftarQuery[2]);
					}
					else if (jumlah == 6)
					{
						int index = findColumn(buatTable, daftarQuery[4]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
							continue;
						}
						printf("index  = %d\n", index);
						deleteTableWhere(buatTable, index, daftarQuery[4], daftarQuery[5]);
					}
					else
					{
						char peringatan[] = "Input Salah";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char peringatan[] = "Data telah dihapus";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}

                else if (strcmp(perintah[0], "select") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
					if (jumlah == 4)
					{
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[3]);
						printf("buat table = %s", buatTable);
						char perintahKolom[1000];
						printf("masuk 4\n");
						if (strcmp(daftarQuery[1], "*") == 0)
						{
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char dataBuffers[40000];
							char sendDatabase[40000];
							bzero(dataBuffer, sizeof(dataBuffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(dataBuffers, sizeof dataBuffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									char padding[20000];
									snprintf(padding, sizeof padding, "%s\t", user.data[i]);
									strcat(dataBuffers, padding);
								}
								strcat(sendDatabase, dataBuffers);
							}
							send(clientSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(dataBuffer, sizeof(dataBuffer));
							fclose(fp);
						}
						else
						{
							int index = findColumn(buatTable, daftarQuery[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char dataBuffers[40000];
							char sendDatabase[40000];
							bzero(dataBuffer, sizeof(dataBuffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(dataBuffers, sizeof dataBuffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (i == index)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(dataBuffers, padding);
									}
								}
								strcat(sendDatabase, dataBuffers);
							}
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(clientSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(dataBuffer, sizeof(dataBuffer));
						}
					}
					else if (jumlah == 7 && strcmp(daftarQuery[4], "WHERE") == 0)
					{
						char buatTable[20000];
						snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[3]);
						printf("buat table = %s", buatTable);
						char perintahKolom[1000];
						printf("masuk 4\n");
						if (strcmp(daftarQuery[1], "*") == 0)
						{
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							fp = fopen(buatTable, "rb");
							char dataBuffers[40000];
							char sendDatabase[40000];
							int index = findColumn(buatTable, daftarQuery[5]);
							printf("%d\n", index);
							bzero(dataBuffer, sizeof(dataBuffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(dataBuffers, sizeof dataBuffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (strcmp(user.data[index], daftarQuery[6]) == 0)
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(dataBuffers, padding);
									}
								}
								strcat(sendDatabase, dataBuffers);
							}
							send(clientSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(dataBuffer, sizeof(dataBuffer));
							fclose(fp);
						}
						else
						{
							int index = findColumn(buatTable, daftarQuery[1]);
							printf("%d\n", index);
							FILE *fp, *fp1;
							struct table user;
							int id, found = 0;
							int indexGanti = findColumn(buatTable, daftarQuery[5]);
							fp = fopen(buatTable, "rb");
							char dataBuffers[40000];
							char sendDatabase[40000];
							bzero(dataBuffer, sizeof(dataBuffer));
							bzero(sendDatabase, sizeof(sendDatabase));
							while (1)
							{
								char enter[] = "\n";
								fread(&user, sizeof(user), 1, fp);
								snprintf(dataBuffers, sizeof dataBuffers, "\n");
								if (feof(fp))
								{
									break;
								}
								for (int i = 0; i < user.jumlahkolom; i++)
								{
									if (i == index && (strcmp(user.data[indexGanti], daftarQuery[6]) == 0 || strcmp(user.data[i], daftarQuery[5]) == 0))
									{
										char padding[20000];
										snprintf(padding, sizeof padding, "%s\t", user.data[i]);
										strcat(dataBuffers, padding);
									}
								}
								strcat(sendDatabase, dataBuffers);
							}
							printf("ini send fix\n%s\n", sendDatabase);
							fclose(fp);
							send(clientSocket, sendDatabase, strlen(sendDatabase), 0);
							bzero(sendDatabase, sizeof(sendDatabase));
							bzero(dataBuffer, sizeof(dataBuffer));
						}
					}
					else
					{
						printf("ini query 3 %s", daftarQuery[jumlah - 3]);
						if (strcmp(daftarQuery[jumlah - 3], "WHERE") != 0)
						{
							char buatTable[20000];
							snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[jumlah - 1]);
							printf("buat table = %s", buatTable);
							printf("tanpa where");
							int index[100];
							int iterasi = 0;
							for (int i = 1; i < jumlah - 2; i++)
							{
								index[iterasi] = findColumn(buatTable, daftarQuery[i]);
								printf("%d\n", index[iterasi]);
								iterasi++;
							}
						}
						else if (strcmp(daftarQuery[jumlah - 3], "WHERE") == 0)
						{
							printf("dengan where");
						}
					}
				}

				else if (strcmp(perintah[0], "update") == 0)
				{
					if (currentDatabase[0] == '\0')
					{
						strcpy(currentDatabase, "Anda belum memilih database");
						send(clientSocket, currentDatabase, strlen(currentDatabase), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char daftarQuery[100][10000];
					char copyPerintah[20000];
					snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
					char *tokens;
					tokens = strtok(copyPerintah, "\'(),= ");
					int jumlah = 0;
					while (tokens != NULL)
					{
						strcpy(daftarQuery[jumlah], tokens);
						printf("%s\n", daftarQuery[jumlah]);
						jumlah++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("jumlah = %d\n", jumlah);
					char buatTable[20000];
					snprintf(buatTable, sizeof buatTable, "databases/%s/%s", currentDatabase, daftarQuery[1]);
					if (jumlah == 5)
					{
						printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
						int index = findColumn(buatTable, daftarQuery[3]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
							continue;
						}
						printf("index = %d\n", index);
						updateColumn(buatTable, index, daftarQuery[4]);
					}
					else if (jumlah == 8)
					{
						printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
						int index = findColumn(buatTable, daftarQuery[3]);
						if (index == -1)
						{
							char peringatan[] = "Kolom tidak ditemukan";
							send(clientSocket, peringatan, strlen(peringatan), 0);
							bzero(dataBuffer, sizeof(dataBuffer));
							continue;
						}
						printf("%s\n", daftarQuery[7]);
						int indexGanti = findColumn(buatTable, daftarQuery[6]);
						updateColumnWhere(buatTable, index, daftarQuery[4], indexGanti, daftarQuery[7]);
					}
					else
					{
						char peringatan[] = "Data telah dihapus";
						send(clientSocket, peringatan, strlen(peringatan), 0);
						bzero(dataBuffer, sizeof(dataBuffer));
						continue;
					}
					char peringatan[] = "Data telah diupdate";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}


				else if (strcmp(perintah[0], "log") == 0)
				{
					writelog(perintah[1], perintah[2]);
					char peringatan[] = "\n";
					send(clientSocket, peringatan, strlen(peringatan), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}
				if (strcmp(dataBuffer, ":exit") == 0)
				{
					printf("Terputus dari %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}
				else
				{
					printf("Client: %s\n", dataBuffer);
					send(clientSocket, dataBuffer, strlen(dataBuffer), 0);
					bzero(dataBuffer, sizeof(dataBuffer));
				}
			}
		}
	}
	close(clientSocket);
	return 0;
}

int findColumn(const char *tablePath, const char *columnName) {
    FILE *filePointer;
    struct Table tableData;
    int index = -1;

    filePointer = fopen(tablePath, "rb");

    fread(&tableData, sizeof(tableData), 1, filePointer);

    for (int i = 0; i < tableData.jumlahkolom; i++) {
        if (strcmp(tableData.data[i], columnName) == 0) {
            index = i;
            break;
        }
    }

    fclose(filePointer);
    return index;
}

int cekUserExist(const char *username) {
    FILE *filePointer;
    struct Allowed userData;

    filePointer = fopen("../database/databases/user.dat", "rb");

    while (fread(&userData, sizeof(userData), 1, filePointer) == 1) {
        if (strcmp(userData.name, username) == 0) {
            fclose(filePointer);
            return 1;
        }
    }

    fclose(filePointer);
    return 0;
}

void createUser(const char *nama, const char *password) {
    struct Allowed newUser;
    strcpy(newUser.name, nama);
    strcpy(newUser.password, password);

    printf("Nama Pengguna: %s\n", newUser.name);
    printf("Password: %s\n", newUser.password);

    FILE *filePointer = fopen("databases/user.dat", "ab");

    fwrite(&newUser, sizeof(newUser), 1, filePointer);

    fclose(filePointer);
}

int cekAllowedDatabase(const char *nama, const char *database) {
    FILE *filePointer;
    struct AllowedDatabase user;

    printf("Nama: %s, Database: %s\n", nama, database);

    filePointer = fopen("../database/databases/permission.dat", "rb");

    while (1) {
        fread(&user, sizeof(user), 1, filePointer);
        if (strcmp(user.name, nama) == 0 && strcmp(user.database, database) == 0) {
            fclose(filePointer);
            return 1; 
        }
        if (feof(filePointer)) {
            break;
        }
    }

    fclose(filePointer);
    return 0; 
}

void insertPermission(const char *nama, const char *database) {
    struct AllowedDatabase user;
    strcpy(user.name, nama);
    strcpy(user.database, database);

    printf("%s %s\n", user.name, user.database);

    char fname[] = {"databases/permission.dat"};
    FILE *filePointer;

    filePointer = fopen(fname, "ab");

    fwrite(&user, sizeof(user), 1, filePointer);

    fclose(filePointer);
}

int deleteTable(const char *table, const char *namaTable) {
    FILE *fileTable, *fileTemp;
    struct Table user;
    int found = 0;

    fileTable = fopen(table, "rb");
    if (fileTable == NULL) {
        perror("Error membuka file tabel");
        return 0;
    }

    fileTemp = fopen("temp", "ab");
    if (fileTemp == NULL) {
        perror("Error membuka file temp");
        fclose(fileTable);
        return 0;
    }

    while (fread(&user, sizeof(user), 1, fileTable) == 1) {
        if (strcmp(user.data[0], namaTable) != 0) {
            fwrite(&user, sizeof(user), 1, fileTemp);
        } else {
            found = 1; 
        }
    }

    fclose(fileTable);
    fclose(fileTemp);
    remove(table);
    rename("temp", table);

    return found;
}

int deleteTableWhere(const char *table, int index, const char *kolom, const char *where) {
    FILE *fileTable, *fileTemp;
    struct Table user;
    int found = 0;
    int datake = 0;

    fileTable = fopen(table, "rb");
    if (fileTable == NULL) {
        perror("Error membuka file tabel");
        return 0;
    }

    fileTemp = fopen("temp", "ab");
    if (fileTemp == NULL) {
        perror("Error membuka file temp");
        fclose(fileTable);
        return 0;
    }

    while (fread(&user, sizeof(user), 1, fileTable) == 1) {
        found = 0;
        struct Table userCopy;
        int iterasi = 0;

        for (int i = 0; i < user.jumlahkolom; i++) {
            if (i == index && datake != 0 && strcmp(user.data[i], where) == 0) {
                found = 1; 
            }

            strcpy(userCopy.data[iterasi], user.data[i]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }

        userCopy.jumlahkolom = user.jumlahkolom;

        if (found != 1) {
            fwrite(&userCopy, sizeof(userCopy), 1, fileTemp);
        }

        datake++;
    }

    fclose(fileTable);
    fclose(fileTemp);
    remove(table);
    rename("temp", table);

    return found;
}

int updateColumn(const char *table, int index, const char *ganti) {
    FILE *fileTable, *fileTemp;
    struct Table user;
    int found = 0;
    int datake = 0;

    fileTable = fopen(table, "rb");
    if (fileTable == NULL) {
        perror("Error membuka file tabel");
        return 0;
    }

    fileTemp = fopen("temp", "ab");
    if (fileTemp == NULL) {
        perror("Error membuka file temp");
        fclose(fileTable);
        return 0;
    }

    while (fread(&user, sizeof(user), 1, fileTable) == 1) {
        struct Table userCopy;
        int iterasi = 0;

        for (int i = 0; i < user.jumlahkolom; i++) {
            if (i == index && datake != 0) {
                strcpy(userCopy.data[iterasi], ganti);
            } else {
                strcpy(userCopy.data[iterasi], user.data[i]);
            }

            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }

        userCopy.jumlahkolom = user.jumlahkolom;

        fwrite(&userCopy, sizeof(userCopy), 1, fileTemp);
        datake++;
    }

    fclose(fileTable);
    fclose(fileTemp);
    remove(table);
    rename("temp", table);

    return datake > 0 ? 1 : 0;
}

int deleteColumn(const char *table, int index) {
    FILE *fileTable, *fileTemp;
    struct Table user;
    int found = 0;

    fileTable = fopen(table, "rb");
    if (fileTable == NULL) {
        perror("Error membuka file tabel");
        return 0;
    }

    fileTemp = fopen("temp", "wb");
    if (fileTemp == NULL) {
        perror("Error membuka file temp");
        fclose(fileTable);
        return 0;
    }

    while (fread(&user, sizeof(user), 1, fileTable) == 1) {
        struct Table userCopy;
        int iterasi = 0;

        for (int i = 0; i < user.jumlahkolom; i++) {
            if (i == index) {
                continue; 
            }

            strcpy(userCopy.data[iterasi], user.data[i]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }

        userCopy.jumlahkolom = user.jumlahkolom - 1;

        fwrite(&userCopy, sizeof(userCopy), 1, fileTemp);
    }

    fclose(fileTable);
    fclose(fileTemp);
    remove(table);
    rename("temp", table);

    return 1;
}

void writelog(char *perintah, char *nama) {
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    char infoWriteLog[1000];

    FILE *file;
    file = fopen("logUser.log", "ab");
    if (file == NULL) {
        perror("Error membuka file logUser.log");
        return;
    }

    sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);

    fputs(infoWriteLog, file);

    fclose(file);
    return;
}

int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where) {
    FILE *fp, *fp1;
    struct table user;

    fp = fopen(table, "rb");
    if (fp == NULL) {
        perror("Error membuka file tabel");
        return -1;  
    }

    fp1 = fopen("temp", "ab");
    if (fp1 == NULL) {
        perror("Error membuka file sementara");
        fclose(fp);
        return -1;  
    }

    int datake = 0;
    while (1) {
        fread(&user, sizeof(user), 1, fp);

        if (feof(fp)) {
            break;
        }

        struct table userCopy;
        int iterasi = 0;

        for (int i = 0; i < user.jumlahkolom; i++) {
            if (i == index && datake != 0 && strcmp(user.data[indexGanti], where) == 0) {
                strcpy(userCopy.data[iterasi], ganti);
            } else {
                strcpy(userCopy.data[iterasi], user.data[i]);
            }

            printf("%s\n", userCopy.data[iterasi]);

            strcpy(userCopy.type[iterasi], user.type[i]);

            printf("%s\n", userCopy.data[iterasi]);

            iterasi++;
        }

        userCopy.jumlahkolom = user.jumlahkolom;

        fwrite(&userCopy, sizeof(userCopy), 1, fp1);
        datake++;
    }

    fclose(fp);
    fclose(fp1);
    remove(table);
    rename("temp", table);

    return 0;  
}