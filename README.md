# Final Praktikum

**Anggota Kelompok:**

| Nama                 | NRP        |
| -------------------- | ---------- |
| Rahmad Aji Wicaksono | 5027221034 |
| Alma Amira Dewani    | 5027221054 |
| Zidny Ilman Na'fian  | 5027221072 |

# A. Autentikasi

Terdapat user dengan username dan password yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.

| Format                                                  | Contoh                                 |
| ------------------------------------------------------- | -------------------------------------- |
| ./[program_client_database] -u [username] -p [password] | ./client_databaseku -u john -p john123 |

Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.

User root sudah ada dari awal.

| Format                                                 | Contoh                                      |
| ------------------------------------------------------ | ------------------------------------------- |
| CREATE USER [nama_user] IDENTIFIED BY [password_user]; | CREATE USER khonsu IDENTIFIED BY khonsu123; |

## Kode

### client.c

```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct allowed
{
	char name[10000];
	char password[10000];
};

// DEKLARASI FUNGSI
int cekAllowed(char *username, char *password);

void writelog(char *perintah, char *nama);

int main(int argc, char *argv[])
{
	int allowed = 0;

	int id_user = geteuid();

	char database_used[1000];

	if (geteuid() == 0)
	{
		allowed = 1;
	}

	else
	{
		int id = geteuid();
		allowed = cekAllowed(argv[2], argv[4]);
	}

	if (allowed == 0)
	{
		return 0;
	}

	int clientSocket, ret;

	struct sockaddr_in serverAddr;

	char buffer[32000];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);

	if (clientSocket < 0)
	{
		printf("Koneksi gagal, silakan coba lagi!!\n");
		exit(1);
	}

	printf("Socket Client telah dibuat.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;

	serverAddr.sin_port = htons(PORT);

	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
	if (ret < 0)
	{
		printf("Koneksi gagal, silakan coba lagi!!\n");
		exit(1);
	}
	printf("Terhubung ke server.\n");

	while (1)
	{
		printf("Enter command (EXIT for close): ");
		char input[10000];
		char copyinput[10000];
		char perintah[100][10000];
		char *token;
		int i = 0;
		scanf(" %[^\n]s", input);
		strcpy(copyinput, input);
		token = strtok(input, " ");
		while (token != NULL)
		{
			strcpy(perintah[i], token);
			i++;
			token = strtok(NULL, " ");
		}
		if (strcmp(perintah[0], "EXIT") == 0)
		{
			snprintf(buffer, sizeof buffer, "%s", perintah[0]);
			send(clientSocket, buffer, strlen(buffer), 0);
			break;
		}
		int wrongCommand = 0;

		if (strcmp(perintah[0], "CREATE") == 0)
		{
			if (strcmp(perintah[1], "USER") == 0 && strcmp(perintah[3], "IDENTIFIED") == 0 && strcmp(perintah[4], "BY") == 0)
			{
				snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", perintah[2], perintah[5], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			}

```

### database.c

```bash
void createUser(char *nama, char *password)
{
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[] = {"databases/user.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}

```

## Penjelasan Kode

### client.c

1. **Header Files**: Program ini menyertakan perpustakaan standar C untuk input/output, alokasi memori, manipulasi string, pemrograman soket, operasi berkas, dan fungsionalitas sistem lainnya.
2. **Definisi Struktur**: Sebuah struktur bernama `allowed` didefinisikan untuk menyimpan informasi tentang seorang pengguna, termasuk nama dan kata sandi.

3. **Deklarasi Fungsi**:

   - `cekAllowed`: Memeriksa apakah seorang pengguna diizinkan berdasarkan username dan password yang diberikan.
   - `writelog`: Menulis log, tetapi implementasi sebenarnya tidak disertakan.

4. **Fungsi Utama**:

   - Menginisialisasi variabel, termasuk `id_user` yang mewakili ID pengguna efektif.
   - Memeriksa apakah program dijalankan dengan hak istimewa root. Jika ya, mengatur `allowed` menjadi 1; jika tidak, memeriksa izin pengguna menggunakan fungsi `cekAllowed`.

5. **Inisialisasi Soket**: Membuat soket menggunakan fungsi `socket()` dan menangani kesalahan. Menginisialisasi struktur alamat server dan mencoba untuk terhubung ke server menggunakan fungsi `connect()`.

6. **Pemrosesan Input Pengguna**: Memasuki loop di mana pengguna diminta untuk memasukkan perintah. Input dipotong menjadi token dan disimpan dalam array 2D untuk pemrosesan lebih lanjut.

7. **Kondisi Keluar**: Jika pengguna memasukkan "EXIT," program mengirim pesan yang sesuai ke server dan keluar dari loop, efektif mengakhiri program.

8. **Pembersihan Soket**: Menutup koneksi soket ketika program keluar.

### database.c

1. `void createUser(char *nama, char *password)`: Ini adalah deklarasi fungsi `createUser`. Fungsi ini menerima dua argumen, yaitu `nama` (username) dan `password` (kata sandi), dan tidak mengembalikan nilai (`void`).

2. `struct allowed user;`: Membuat sebuah struktur `allowed` bernama `user` yang digunakan untuk menyimpan informasi pengguna, seperti nama dan kata sandi.

3. `strcpy(user.name, nama);` dan `strcpy(user.password, password);`: Menyalin nilai dari argumen `nama` ke atribut `name` dari struktur `user` dan nilai dari argumen `password` ke atribut `password` dari struktur `user`. Ini mengisi data pengguna ke dalam struktur.

4. `printf("%s %s\n", user.name, user.password);`: Mencetak nilai `name` dan `password` dari struktur `user` ke layar. Ini bertujuan untuk memastikan bahwa data pengguna telah disalin dengan benar ke dalam struktur sebelum disimpan ke dalam berkas.

5. `char fname[] = {"databases/user.dat"};`: Mendeklarasikan nama berkas (`fname`) yang akan digunakan untuk menyimpan informasi pengguna. Berkas ini disimpan dalam direktori "databases" dengan nama "user.dat".

6. `FILE *fp;`: Membuat pointer berkas (`fp`) yang akan digunakan untuk berinteraksi dengan berkas.

7. `fp = fopen(fname, "ab");`: Membuka berkas dengan mode "ab" (append binary), yang berarti berkas akan dibuka untuk penulisan biner dan data baru akan ditambahkan ke akhir berkas (tanpa menghapus yang sudah ada).

8. `fwrite(&user, sizeof(user), 1, fp);`: Menulis data struktur `user` ke dalam berkas. `&user` adalah alamat memori dari struktur `user`, `sizeof(user)` adalah ukuran dari struktur tersebut, dan `1` adalah jumlah elemen yang akan ditulis.

9. `fclose(fp);`: Menutup berkas setelah selesai penulisan.

## Output

![auth](gambar/autentikasi.png)

# B. Autorisasi

Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.

| Format               | Contoh         |
| -------------------- | -------------- |
| USE [nama_database]; | USE database1; |

Yang bisa memberikan permission atas database untuk suatu user hanya root.

| Format                                             | Contoh                                 |
| -------------------------------------------------- | -------------------------------------- |
| GRANT PERMISSION [nama_database] INTO [nama_user]; | GRANT PERMISSION database1 INTO user1; |

User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

## Kode

### client.c

```bash
else if (strcmp(perintah[0], "USE") == 0)
	{
		snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", perintah[1], argv[2], id_user);
		send(clientSocket, buffer, strlen(buffer), 0);
	}

else if (strcmp(perintah[0], "GRANT") == 0 && strcmp(perintah[1], "PERMISSION") == 0 && strcmp(perintah[3], "INTO") == 0)
	{
	snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", perintah[2], perintah[4], id_user);
	send(clientSocket, buffer, strlen(buffer), 0);
	}
```

```bash
int cekAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;

    int id, found = 0;
    fp = fopen("../database/databases/user.dat", "rb");

    while (1) {
        fread(&user, sizeof(user), 1, fp);
        if (strcmp(user.name, username) == 0) {
            if (strcmp(user.password, password) == 0) {
                found = 1;
            }
        }

        if (feof(fp)) {
            break;
        }
    }

    fclose(fp);

    if (found == 0) {
        printf("Anda tidak diizinkan\n");
        return 0;
    } else {
        return 1;
    }
}

```

### database.c

```bash
void insertPermission(char *nama, char *database)
{
	struct allowed_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[] = {"databases/permission.dat"};
	FILE *fp;
	fp = fopen(fname, "ab");
	fwrite(&user, sizeof(user), 1, fp);
	fclose(fp);
}
```

```bash
else if (strcmp(perintah[0], "gPermission") == 0) {
    if (strcmp(perintah[3], "0") == 0) {
        int exist = cekUserExist(perintah[2]);
        if (exist == 1) {
            insertPermission(perintah[2], perintah[1]);
        } else {
            char peringatan[] = "User tidak ditemukan";
            send(newSocket, peringatan, strlen(peringatan), 0);
            bzero(buffer, sizeof(buffer));
        }
    } else {
        char peringatan[] = "Anda tidak diizinkan";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
    }
} else if (strcmp(perintah[0], "uDatabase") == 0) {
    if (strcmp(perintah[3], "0") != 0) {
        int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
        if (allowed != 1) {
            char peringatan[] = "Akses_database : Anda tidak diizinkan";
            send(newSocket, peringatan, strlen(perintatan), 0);
            bzero(buffer, sizeof(buffer));
        } else {
            strncpy(database_used, perintah[1], sizeof(perintah[1]));
            char peringatan[] = "Akses_database : Diizinkan";
            printf("database_used = %s\n", database_used);
            send(newSocket, peringatan, strlen(perintatan), 0);
            bzero(buffer, sizeof(buffer));
        }
    }
}

```

## Penjelasan Kode

### client.c

1. **Perintah USE**:

   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "USE".
   - Membuat pesan buffer dengan menggunakan `snprintf` yang berisi informasi perintah untuk mengganti atau menggunakan database.
   - Mengirimkan pesan tersebut ke server menggunakan fungsi `send`.

2. **Perintah GRANT PERMISSION**:

   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "GRANT PERMISSION".
   - Membuat pesan buffer dengan menggunakan `snprintf` yang berisi informasi perintah untuk memberikan izin akses ke sebuah database.
   - Mengirimkan pesan tersebut ke server menggunakan fungsi `send`.

3. **Fungsi `cekAllowed`**:
   - Fungsi ini digunakan untuk memeriksa apakah seorang pengguna diizinkan berdasarkan username dan password yang diberikan.
   - Membuka berkas "user.dat" dalam mode baca biner (`"rb"`).
   - Membaca data pengguna satu per satu dari berkas dan membandingkan dengan informasi yang diberikan.
   - Jika pengguna dengan username dan password yang sesuai ditemukan, mengeset `found` menjadi 1.
   - Jika akhir berkas (`feof(fp)`) telah tercapai, keluar dari loop.
   - Menutup berkas setelah selesai membaca.
   - Jika `found` tetap 0 (tidak ditemukan), mencetak pesan bahwa pengguna tidak diizinkan dan mengembalikan nilai 0.
   - Jika ditemukan, mengembalikan nilai 1, menandakan bahwa pengguna diizinkan.

### database.c

1. **Fungsi `insertPermission`**:

   - Fungsi ini bertujuan untuk menyisipkan izin (permission) ke dalam berkas "permission.dat".
   - Membuat sebuah struktur `allowed_database` yang menyimpan nama pengguna (`nama`) dan nama database (`database`).
   - Mengisi nilai struktur dengan data yang diberikan.
   - Mencetak nilai `name` dan `database` dari struktur ke layar.
   - Membuka berkas "permission.dat" dalam mode append binary (`"ab"`).
   - Menulis data struktur ke dalam berkas menggunakan `fwrite`.
   - Menutup berkas setelah selesai penulisan.

2. **Perintah "gPermission"**:

   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "gPermission".
   - Memeriksa apakah perintah tersebut memiliki parameter ketiga (`perintah[3]`) yang bernilai "0".
   - Jika ya, melakukan pemeriksaan keberadaan pengguna menggunakan fungsi `cekUserExist`.
     - Jika pengguna ditemukan (`exist == 1`), memanggil fungsi `insertPermission` untuk menyisipkan izin.
     - Jika tidak ditemukan, mengirim pesan ke client bahwa "User tidak ditemukan".
   - Jika parameter ketiga bukan "0", mengirim pesan ke client bahwa "Anda tidak diizinkan".

3. **Perintah "uDatabase"**:
   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "uDatabase".
   - Memeriksa apakah parameter ketiga (`perintah[3]`) tidak sama dengan "0".
   - Jika tidak sama dengan "0", memanggil fungsi `cekAllowedDatabase` untuk memeriksa izin akses database.
     - Jika tidak diizinkan (`allowed != 1`), mengirim pesan ke client bahwa "Akses_database : Anda tidak diizinkan".
     - Jika diizinkan, menyimpan nama database yang sedang digunakan ke dalam variabel `database_used` dan mengirim pesan ke client bahwa "Akses_database : Diizinkan".

## Output

![autor](gambar/autorisasi.png)

# C. Data Definition Language

- Input penamaan database, tabel, dan kolom hanya angka dan huruf.
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.

| Format                                                     | Contoh                                                                      |
| ---------------------------------------------------------- | --------------------------------------------------------------------------- |
| CREATE DATABASE [nama_database];                           | CREATE DATABASE database1;                                                  |
| CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...); | CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int); |
| DROP DATABASE [nama_database];                             | DROP DATABASE database1;                                                    |
| DROP TABLE [nama_tabel];                                   | DROP TABLE table1;                                                          |
| DROP COLUMN [nama_kolom] FROM [nama_tabel];                | DROP COLUMN kolom1 FROM table1;                                             |

## Kode

### client.c

```bash
if (strcmp(perintah[0], "CREATE") == 0) {
    if (strcmp(perintah[1], "TABLE") == 0) {
        snprintf(buffer, sizeof buffer, "cTable:%s", copyinput);
        send(clientSocket, buffer, strlen(buffer), 0);
    } else if (strcmp(perintah[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", perintah[2], argv[2], id_user);
        send(clientSocket, buffer, strlen(buffer), 0);
    }
} else if (strcmp(perintah[0], "DROP") == 0) {
    if (strcmp(perintah[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", perintah[2], argv[2]);
        send(clientSocket, buffer, strlen(buffer), 0);
    } else if (strcmp(perintah[1], "COLUMN") == 0) {
        snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", perintah[2], perintah[4], argv[2]);
        send(clientSocket, buffer, strlen(buffer), 0);
    } else if (strcmp(perintah[1], "TABLE") == 0) {
        snprintf(buffer, sizeof buffer, "dTable:%s:%s", perintah[2], argv[2]);
        send(clientSocket, buffer, strlen(buffer), 0);
    }
}

```

### database.c

```bash
else if (strcmp(perintah[0], "cDatabase") == 0) {
    char lokasi[20000];
    snprintf(lokasi, sizeof lokasi, "databases/%s", perintah[1]);
    printf("lokasi = %s, nama = %s , database = %s\n", lokasi, perintah[2], perintah[1]);
    mkdir(lokasi, 0777);
    insertPermission(perintah[2], perintah[1]);
}

else if (strcmp(perintah[0], "cTable") == 0) {
    printf("%s\n", perintah[1]);
    char *tokens;
    if (database_used[0] == '\0') {
        strcpy(database_used, "Anda belum memilih database");
        send(newSocket, database_used, strlen(database_used), 0);
        bzero(buffer, sizeof(buffer));
    } else {
        char daftarQuery[100][10000];
        char copyPerintah[20000];
        snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
        tokens = strtok(copyPerintah, "(), ");
        int jumlah = 0;

        while (tokens != NULL) {
            strcpy(daftarQuery[jumlah], tokens);
            printf("%s\n", daftarQuery[jumlah]);
            jumlah++;
            tokens = strtok(NULL, "(), ");
        }

        char buatTable[20000];
        snprintf(buatTable, sizeof buatTable, "../database/databases/%s/%s", database_used, daftarQuery[2]);
        int iterasi = 0;
        int iterasiData = 3;
        struct table kolom;

        while (jumlah > 3) {
            strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
            printf("%s\n", kolom.data[iterasi]);
            strcpy(kolom.type[iterasi], daftarQuery[iterasiData + 1]);
            iterasiData = iterasiData + 2;
            jumlah = jumlah - 2;
            iterasi++;
        }

        kolom.jumlahkolom = iterasi;
        printf("iterasi = %d\n", iterasi);
        FILE *fp;
        printf("%s\n", buatTable);
        fp = fopen(buatTable, "ab");
        fwrite(&kolom, sizeof(kolom), 1, fp);
        fclose(fp);
    }
}

else if (strcmp(perintah[0], "dDatabase") == 0) {
    int allowed = cekAllowedDatabase(perintah[2], perintah[1]);

    if (allowed != 1) {
        char peringatan[] = "Akses_database : Anda tidak diizinkan";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    } else {
        char hapus[20000];
        snprintf(hapus, sizeof hapus, "rm -r databases/%s", perintah[1]);
        system(hapus);
        char peringatan[] = "Database telah dihapus";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
    }
}

else if (strcmp(perintah[0], "dTable") == 0) {
    if (database_used[0] == '\0') {
        strcpy(database_used, "Anda belum memilih database");
        send(newSocket, database_used, strlen(database_used), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    }

    char hapus[20000];
    snprintf(hapus, sizeof hapus, "databases/%s/%s", database_used, perintah[1]);
    remove(hapus);
    char peringatan[] = "Table telah dihapus";
    send(newSocket, peringatan, strlen(peringatan), 0);
    bzero(buffer, sizeof(buffer));
}

else if (strcmp(perintah[0], "dColumn") == 0) {
    if (database_used[0] == '\0') {
        strcpy(database_used, "Anda belum memilih database");
        send(newSocket, database_used, strlen(database_used), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    }

    char buatTable[20000];
    snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, perintah[2]);
    int index = findColumn(buatTable, perintah[1]);

    if (index == -1) {
        char peringatan[] = "Kolom tidak ditemukan";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    }

    deleteColumn(buatTable, index);
    char peringatan[] = "Kolom telah dihapus";
    send(newSocket, peringatan, strlen(peringatan), 0);
    bzero(buffer, sizeof(buffer));
}


```

## Penjelasan Kode

### client.c

1. **Perintah "CREATE"**:

   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "CREATE".
   - Memeriksa apakah perintah selanjutnya (`perintah[1]`) adalah "TABLE" atau "DATABASE".
   - Jika "TABLE", maka:
     - Membuat pesan buffer menggunakan `snprintf` yang berisi informasi perintah untuk membuat tabel dan mengirimkannya ke server.
   - Jika "DATABASE", maka:
     - Membuat pesan buffer yang berisi informasi perintah untuk membuat database dan mengirimkannya ke server.

2. **Perintah "DROP"**:
   - Fungsi ini dijalankan jika perintah yang dimasukkan adalah "DROP".
   - Memeriksa apakah perintah selanjutnya (`perintah[1]`) adalah "DATABASE", "COLUMN", atau "TABLE".
   - Jika "DATABASE", maka:
     - Membuat pesan buffer menggunakan `snprintf` yang berisi informasi perintah untuk menghapus database dan mengirimkannya ke server.
   - Jika "COLUMN", maka:
     - Membuat pesan buffer yang berisi informasi perintah untuk menghapus kolom dan mengirimkannya ke server.
   - Jika "TABLE", maka:
     - Membuat pesan buffer yang berisi informasi perintah untuk menghapus tabel dan mengirimkannya ke server.

### database.c

1. **Perintah "cDatabase" (Create Database)**:

   - Jika perintah yang diterima adalah "cDatabase", program akan melakukan beberapa langkah:
     - Membuat string `lokasi` yang berisi path direktori untuk database baru dengan menggunakan `snprintf`.
     - Menggunakan fungsi `mkdir` untuk membuat direktori baru sesuai dengan path yang telah dibuat.
     - Mencetak informasi ke layar mengenai lokasi, nama, dan database yang baru dibuat.
     - Memanggil fungsi `insertPermission` untuk memberikan izin akses kepada pengguna terkait ke database baru.

2. **Perintah "cTable" (Create Table)**:

   - Jika perintah yang diterima adalah "cTable", program akan melakukan beberapa langkah:
     - Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`.
     - Mengurai perintah untuk mendapatkan informasi tentang tabel yang akan dibuat.
     - Membuat path untuk tabel baru dengan menggunakan `snprintf`.
     - Menggunakan token untuk membaca informasi mengenai kolom dan tipe data yang dimasukkan oleh pengguna.
     - Membuat berkas tabel dan menulis informasi kolom ke dalamnya.
     - Perlu dicatat bahwa dalam implementasi ini, struktur `table` digunakan untuk menyimpan informasi kolom dan tipe data dari tabel.

3. **Perintah "dDatabase" (Drop Database)**:

   - Jika perintah yang diterima adalah "dDatabase", program akan melakukan beberapa langkah:
     - Memeriksa izin akses pengguna terkait menggunakan fungsi `cekAllowedDatabase`.
     - Jika pengguna diizinkan, menggunakan perintah sistem (`system("rm -r ...")`) untuk menghapus seluruh direktori database.
     - Mengirim pesan ke client terkait hasil penghapusan database.

4. **Perintah "dTable" (Drop Table)**:

   - Jika perintah yang diterima adalah "dTable", program akan melakukan beberapa langkah:
     - Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`.
     - Menghapus berkas tabel sesuai dengan perintah menggunakan fungsi `remove`.
     - Mengirim pesan ke client terkait hasil penghapusan tabel.

5. **Perintah "dColumn" (Drop Column)**:
   - Jika perintah yang diterima adalah "dColumn", program akan melakukan beberapa langkah:
     - Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`.
     - Membuat path untuk tabel yang akan dihapus kolomnya.
     - Mencari indeks kolom yang akan dihapus menggunakan fungsi `findColumn`.
     - Jika kolom ditemukan, menggunakan fungsi `deleteColumn` untuk menghapus kolom dari berkas tabel.
     - Mengirim pesan ke client terkait hasil penghapusan kolom.

## Output

### create database

![cdb](gambar/create-db.png)

### Create table

![ctable](gambar/create-table.png)

### drop kolom

![dkolom](gambar/drop-kolom.png)

### drop table

![dtable](gambar/drop-table.png)

### drop database

![ddb](gambar/drop-database.png)

# D. Data Manipulation Language

- Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
- Hanya bisa update satu kolom per satu command.
- Delete data yang ada di tabel.
- Select data
- Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.

| Format                                        | Contoh                                         |
| --------------------------------------------- | ---------------------------------------------- |
| INSERT INTO [nama_tabel] ([value], ...);      | INSERT INTO table1 (‘value1’, 2, ‘value3’, 4); |
| UPDATE [nama_tabel] SET [nama_kolom]=[value]; | UPDATE table1 SET kolom1=’new_value1’;         |
| DELETE FROM [nama_tabel];                     | DELETE FROM table1;                            |
| SELECT [nama_kolom, … *] FROM [nama_tabel];   | SELECT \* FROM table1;                         |

## Kode dan Penjelasan

### 1. INSERT

#### client.c

```bash
else if (strcmp(perintah[0], "INSERT") == 0 && strcmp(perintah[1], "INTO") == 0)
		{
			snprintf(buffer, sizeof buffer, "insert:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
		}

```

**Penjelasan:**

Jika perintah yang diterima adalah "INSERT INTO", program akan melakukan beberapa langkah:

- Menggunakan `snprintf` untuk membuat pesan buffer yang berisi informasi perintah `INSERT INTO` dengan data yang diinputkan oleh pengguna.
- Mengirim pesan buffer tersebut ke server menggunakan fungsi `send` dan soket client (`clientSocket`).
- Pesan buffer ini kemudian akan diproses oleh server untuk melakukan operasi `INSERT` ke dalam tabel yang sesuai.

#### database.c

```bash
else if (strcmp(perintah[0], "insert") == 0) {
    if (database_used[0] == '\0') {
        strcpy(database_used, "Anda belum memilih database");
        send(newSocket, database_used, strlen(database_used), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    }

    char daftarQuery[100][10000];
    char copyPerintah[20000];
    snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
    char *tokens;
    tokens = strtok(copyPerintah, "\'(), ");
    int jumlah = 0;

    while (tokens != NULL) {
        strcpy(daftarQuery[jumlah], tokens);
        jumlah++;
        tokens = strtok(NULL, "\'(), ");
    }

    char buatTable[20000];
    snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
    FILE *fp;
    int banyakKolom;
    fp = fopen(buatTable, "r");

    if (fp == NULL) {
        char peringatan[] = "Table tidak ditemukan";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    } else {
        struct table user;
        fread(&user, sizeof(user), 1, fp);
        banyakKolom = user.jumlahkolom;
        fclose(fp);
    }

    int iterasi = 0;
    int iterasiData = 3;
    struct table kolom;

    while (jumlah > 3) {
        strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
        printf("%s\n", kolom.data[iterasi]);
        strcpy(kolom.type[iterasi], "string");
        iterasiData++;
        jumlah = jumlah - 1;
        iterasi++;
    }

    kolom.jumlahkolom = iterasi;

    if (banyakKolom != kolom.jumlahkolom) {
        char peringatan[] = "Input Anda tidak cocok dengan kolom";
        send(newSocket, peringatan, strlen(peringatan), 0);
        bzero(buffer, sizeof(buffer));
        continue;
    }

    printf("iterasi = %d\n", iterasi);
    FILE *fp1;
    printf("%s\n", buatTable);
    fp1 = fopen(buatTable, "ab");
    fwrite(&kolom, sizeof(kolom), 1, fp1);
    fclose(fp1);
    char peringatan[] = "Data telah dimasukkan";
    send(newSocket, peringatan, strlen(peringatan), 0);
    bzero(buffer, sizeof(buffer));
}

```

**Penjelasan:**

Jika perintah yang diterima adalah "insert", program akan melakukan beberapa langkah:

- Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`. Jika belum, mengirim pesan ke client bahwa database belum dipilih.
- Mengurai perintah untuk mendapatkan informasi mengenai data yang akan dimasukkan ke dalam tabel.
- Membuat path untuk tabel yang dituju dengan menggunakan `snprintf`.
- Membuka berkas tabel dalam mode baca (`"r"`) untuk memeriksa jumlah kolom yang ada di tabel.
- Jika tabel tidak ditemukan, mengirim pesan ke client bahwa tabel tidak ditemukan.
- Jika tabel ditemukan, membaca informasi jumlah kolom dari tabel.
- Membaca data yang akan dimasukkan ke dalam tabel dan menyimpannya dalam struktur `table`.
- Memeriksa apakah jumlah kolom yang akan dimasukkan sesuai dengan jumlah kolom di tabel.
- Jika tidak sesuai, mengirim pesan ke client bahwa input tidak cocok dengan kolom di tabel.
- Jika sesuai, membuka berkas tabel dalam mode append binary (`"ab"`) dan menulis data ke dalam tabel.
- Mengirim pesan ke client bahwa data telah dimasukkan.

#### Output

![insert](gambar/insert.png)

### 2. UPDATE

#### client.c

```bash
else if (strcmp(perintah[0], "UPDATE") == 0)
		{
			snprintf(buffer, sizeof buffer, "update:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
		}

```

**Penjelasan:**

Jika perintah yang diterima adalah "UPDATE", program akan melakukan beberapa langkah:

- Menggunakan `snprintf` untuk membuat pesan buffer yang berisi informasi perintah `UPDATE` dengan data yang diinputkan oleh pengguna.
- Mengirim pesan buffer tersebut ke server menggunakan fungsi `send` dan soket client (`clientSocket`).
- Pesan buffer ini kemudian akan diproses oleh server untuk melakukan operasi `UPDATE` ke dalam tabel yang sesuai.

#### database.c

```bash
else if (strcmp(perintah[0], "update") == 0){
	if (database_used[0] == '\0')
	{
		strcpy(database_used, "Anda belum memilih database");
		send(newSocket, database_used, strlen(database_used), 0);
		bzero(buffer, sizeof(buffer));
		continue;
	}
		char daftarQuery[100][10000];
		char copyPerintah[20000];
		snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
		char *tokens;
		tokens = strtok(copyPerintah, "\'(),= ");
		int jumlah = 0;
		while (tokens != NULL)
	{
		strcpy(daftarQuery[jumlah], tokens);
		printf("%s\n", daftarQuery[jumlah]);
		jumlah++;
		tokens = strtok(NULL, "\'(),= ");
	}
		printf("jumlah = %d\n", jumlah);
		char buatTable[20000];
		snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[1]);
		if (jumlah == 5)
	{
		printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
		int index = findColumn(buatTable, daftarQuery[3]);
		if (index == -1)
			{
			char peringatan[] = "Kolom tidak ditemukan";
			send(newSocket, peringatan, strlen(peringatan), 0);
			bzero(buffer, sizeof(buffer));
			continue;
			}
			printf("index = %d\n", index);
			updateColumn(buatTable, index, daftarQuery[4]);
	}
	else if (jumlah == 8)
	{
		printf("buat table = %s, kolumn = %s", buatTable, daftarQuery[3]);
		int index = findColumn(buatTable, daftarQuery[3]);
		if (index == -1)
			{
				char peringatan[] = "Kolom tidak ditemukan";
			    send(newSocket, peringatan, strlen(peringatan), 0);
				bzero(buffer, sizeof(buffer));
				continue;
			}
			    printf("%s\n", daftarQuery[7]);
				int indexGanti = findColumn(buatTable, daftarQuery[6]);
				updateColumnWhere(buatTable, index, daftarQuery[4], indexGanti, daftarQuery[7]);
	}
	else
	{
		char peringatan[] = "Data telah dihapus";
		send(newSocket, peringatan, strlen(peringatan), 0);
		bzero(buffer, sizeof(buffer));
		continue;
	}
		char peringatan[] = "Data telah diupdate";
		send(newSocket, peringatan, strlen(peringatan), 0);
		zero(buffer, sizeof(buffer));
	}

```

**Penjelasan:**

Jika perintah yang diterima adalah "update", program akan melakukan beberapa langkah:

- Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`. Jika belum, mengirim pesan ke client bahwa database belum dipilih.
- Mengurai perintah untuk mendapatkan informasi mengenai kolom dan nilai yang akan diupdate dalam tabel.
- Membuat path untuk tabel yang akan diupdate dengan menggunakan `snprintf`.
- Jika jumlah elemen dalam perintah adalah 5 atau 8, program akan melanjutkan, jika tidak, mengirim pesan ke client bahwa data telah dihapus.
- Jika jumlah elemen adalah 5, program akan mencari indeks kolom yang sesuai dengan nama kolom yang akan diupdate dengan menggunakan fungsi `findColumn`.
- Jika kolom tidak ditemukan, mengirim pesan ke client bahwa kolom tidak ditemukan.
- Jika kolom ditemukan, menggunakan fungsi `updateColumn` untuk mengupdate nilai dalam kolom tersebut.
- Jika jumlah elemen adalah 8, program akan mencari indeks kolom yang sesuai dengan nama kolom yang akan diupdate dan nama kolom yang digunakan sebagai kondisi WHERE.
- Jika salah satu kolom tidak ditemukan, mengirim pesan ke client bahwa kolom tidak ditemukan.
- Jika kolom ditemukan, menggunakan fungsi `updateColumnWhere` untuk mengupdate nilai dalam kolom tersebut berdasarkan kondisi WHERE.
- Mengirim pesan ke client bahwa data telah diupdate.

#### Output

![delete](gambar/update.png)

### 3. DELETE

#### client.c

```bash
else if (strcmp(perintah[0], "DELETE") == 0)
		{
			snprintf(buffer, sizeof buffer, "delete:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
		}

```

**Penjelasan:**

Jika perintah yang diterima adalah "DELETE", program akan melakukan beberapa langkah:

- Menggunakan `snprintf` untuk membuat pesan buffer yang berisi informasi perintah `DELETE` dengan data yang diinputkan oleh pengguna.
- Mengirim pesan buffer tersebut ke server menggunakan fungsi `send` dan soket client (`clientSocket`).
- Pesan buffer ini kemudian akan diproses oleh server untuk melakukan operasi `DELETE` pada tabel yang sesuai.

#### database.c

```bash
else if (strcmp(perintah[0], "delete") == 0){
	if (database_used[0] == '\0'){
		strcpy(database_used, "Anda belum memilih database");
		send(newSocket, database_used, strlen(database_used), 0);
		bzero(buffer, sizeof(buffer));
		continue;
	}
		char daftarQuery[100][10000];
		char copyPerintah[20000];
		snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
		char *tokens;
		tokens = strtok(copyPerintah, "\'(),= ");
		int jumlah = 0;
		while (tokens != NULL)
	{
		strcpy(daftarQuery[jumlah], tokens);
		printf("%s\n", daftarQuery[jumlah]);
		jumlah++;
		tokens = strtok(NULL, "\'(),= ");
	}
		printf("jumlah = %d\n", jumlah);
		char buatTable[20000];
		snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, daftarQuery[2]);
		if (jumlah == 3){
			deleteTable(buatTable, daftarQuery[2]);
		}
		else if (jumlah == 6)
			{
			int index = findColumn(buatTable, daftarQuery[4]);
			if (index == -1){
				char peringatan[] = "Kolom tidak ditemukan";
				send(newSocket, peringatan, strlen(peringatan), 0);
				bzero(buffer, sizeof(buffer));
				continue;
				}
			    printf("index  = %d\n", index);
				deleteTableWhere(buatTable, index, daftarQuery[4], daftarQuery[5]);
				}
			else
				{
				char peringatan[] = "Input Salah";
				send(newSocket, peringatan, strlen(peringatan), 0);
				bzero(buffer, sizeof(buffer));
				continue;
			}
				char peringatan[] = "Data telah dihapus";
				send(newSocket, peringatan, strlen(peringatan), 0);
				bzero(buffer, sizeof(buffer));
			}

```

```bash
int deleteTable(char *table, char *namaTable)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	fread(&user, sizeof(user), 1, fp);
	int index = -1;
	struct table userCopy;
	for (int i = 0; i < user.jumlahkolom; i++)
	{
		strcpy(userCopy.data[i], user.data[i]);
		strcpy(userCopy.type[i], user.type[i]);
	}
	userCopy.jumlahkolom = user.jumlahkolom;
	fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 1;
}

int deleteTableWhere(char *table, int index, char *kolom, char *where)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		found = 0;
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[i], where) == 0)
			{
				found = 1;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom;
		if (found != 1)
		{
			fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		}
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteColumn(char *table, int index)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "wb");
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index)
			{
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom - 1;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

```

**Penjelasan:**

1. **Perintah "delete"**:

   - Jika perintah yang diterima adalah "delete", program akan melakukan beberapa langkah:
     - Mengecek apakah pengguna sudah memilih database dengan melihat nilai `database_used`. Jika belum, mengirim pesan ke client bahwa database belum dipilih.
     - Mengurai perintah untuk mendapatkan informasi mengenai tabel, kolom, dan kondisi WHERE yang akan dihapus dari tabel.
     - Membuat path untuk tabel yang akan dihapus dengan menggunakan `snprintf`.
     - Jika jumlah elemen dalam perintah adalah 3, program akan menghapus seluruh tabel menggunakan fungsi `deleteTable`.
     - Jika jumlah elemen adalah 6, program akan mencari indeks kolom yang sesuai dengan nama kolom yang digunakan sebagai kondisi WHERE.
     - Jika kolom tidak ditemukan, mengirim pesan ke client bahwa kolom tidak ditemukan.
     - Jika kolom ditemukan, menggunakan fungsi `deleteTableWhere` untuk menghapus baris-baris dalam tabel berdasarkan kondisi WHERE.
     - Mengirim pesan ke client bahwa data telah dihapus.

2. **Fungsi `deleteTable`**:

   - Fungsi ini digunakan untuk menghapus seluruh tabel.
   - Membaca data dari tabel yang ada, menyalin data ke tabel sementara, dan kemudian menghapus dan mengganti tabel yang lama dengan tabel sementara.

3. **Fungsi `deleteTableWhere`**:

   - Fungsi ini digunakan untuk menghapus baris-baris tertentu dalam tabel berdasarkan kondisi WHERE.
   - Membaca data dari tabel yang ada, menyalin data yang memenuhi kondisi ke tabel sementara, dan kemudian menghapus dan mengganti tabel yang lama dengan tabel sementara.

4. **Fungsi `deleteColumn`**:
   - Fungsi ini digunakan untuk menghapus kolom tertentu dalam tabel.
   - Membaca data dari tabel yang ada, menyalin data tanpa kolom yang akan dihapus ke tabel sementara, dan kemudian menghapus dan mengganti tabel yang lama dengan tabel sementara.

#### Output

![delete](gambar/delete.png)

### 4. SELECT

#### client.c

```
else if (strcmp(perintah[0], "SELECT") == 0)
		{
			snprintf(buffer, sizeof buffer, "select:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
		}

```

**Penjelasan:**

Jika perintah yang diterima adalah "SELECT", program akan melakukan beberapa langkah:

- Menggunakan `snprintf` untuk membuat pesan buffer yang berisi informasi perintah `SELECT` dengan data yang diinputkan oleh pengguna.
- Mengirim pesan buffer tersebut ke server menggunakan fungsi `send` dan soket client (`clientSocket`).
- Pesan buffer ini kemudian akan diproses oleh server untuk menjalankan perintah `SELECT` pada database.

### 5. WHERE

#### database.c

```bash
int updateColumnWhere(char *table, int index, char *ganti, int indexGanti, char *where)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[indexGanti], where) == 0)
			{
				strcpy(userCopy.data[iterasi], ganti);
			}
			else
			{
				strcpy(userCopy.data[iterasi], user.data[i]);
			}
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom;
		fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```

**Penjelasan:**

- `table`: Path ke file tabel yang akan diupdate.
- `index`: Indeks kolom yang akan diupdate.
- `ganti`: Nilai baru yang akan dimasukkan ke dalam kolom.
- `indexGanti`: Indeks kolom yang akan diupdate berdasarkan kondisi WHERE.
- `where`: Nilai yang harus sama agar operasi update dilakukan.
- `FILE *fp, *fp1`: File pointer untuk membaca dan menulis pada file tabel.
- `struct table user`: Struktur data untuk menyimpan informasi tabel.
- `int id, found = 0`: Variabel untuk membantu dalam penanganan kondisi.
- Membuka file tabel (`fp`) untuk dibaca dan membuat file sementara (`fp1`) untuk menulis.
- Membaca setiap entri dalam tabel menggunakan loop while.
- Jika mencapai akhir file (`feof(fp)`), proses loop dihentikan.
- Setiap baris tabel dibaca ke dalam `user`.
- Membuat `userCopy` sebagai salinan dari `user` untuk dimodifikasi.
- Iterasi melalui kolom-kolom dalam baris dan memproses kondisi:
  - Jika iterasi mencapai indeks kolom yang akan diupdate (`index`) dan bukan iterasi pertama (`datake != 0`) serta nilai pada kolom yang digunakan sebagai kondisi WHERE (`user.data[indexGanti]`) sama dengan nilai kondisi (`where`), maka nilai kolom tersebut diganti dengan nilai baru (`ganti`).
  - Jika tidak memenuhi kondisi, nilai kolom disalin dari `user`.
- Menulis baris yang sudah dimodifikasi (`userCopy`) ke dalam file sementara (`fp1`).
- Meningkatkan `datake` untuk menandai iterasi ke berapa.
- Menutup file asli (`fp`) dan file sementara (`fp1`).
- Menghapus file asli (`remove(table)`) dan mengganti dengan file sementara (`rename("temp", table)`).
- Fungsi mengembalikan nilai 0.

```bash
int deleteTableWhere(char *table, int index, char *kolom, char *where)
{
	FILE *fp, *fp1;
	struct table user;
	int id, found = 0;
	fp = fopen(table, "rb");
	fp1 = fopen("temp", "ab");
	int datake = 0;
	while (1)
	{
		found = 0;
		fread(&user, sizeof(user), 1, fp);
		if (feof(fp))
		{
			break;
		}
		struct table userCopy;
		int iterasi = 0;
		for (int i = 0; i < user.jumlahkolom; i++)
		{
			if (i == index && datake != 0 && strcmp(user.data[i], where) == 0)
			{
				found = 1;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			printf("%s\n", userCopy.data[iterasi]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			printf("%s\n", userCopy.data[iterasi]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom;
		if (found != 1)
		{
			fwrite(&userCopy, sizeof(userCopy), 1, fp1);
		}
		datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

```

**Penjelasan:**

- `table`: Path ke file tabel yang akan dihapus barisnya.
- `index`: Indeks kolom yang digunakan sebagai kondisi WHERE.
- `kolom`: Nama kolom yang digunakan sebagai kondisi WHERE.
- `where`: Nilai yang harus sama agar baris dihapus.
- `FILE *fp, *fp1`: File pointer untuk membaca dan menulis pada file tabel.
- `struct table user`: Struktur data untuk menyimpan informasi tabel.
- `int id, found = 0`: Variabel untuk membantu dalam penanganan kondisi.
- `int datake = 0`: Variabel untuk melacak iterasi ke berapa dalam file tabel.
- Membuka file tabel (`fp`) untuk dibaca dan membuat file sementara (`fp1`) untuk menulis.
- Membaca setiap entri dalam tabel menggunakan loop while.
- Jika mencapai akhir file (`feof(fp)`), proses loop dihentikan.
- Setiap baris tabel dibaca ke dalam `user`.
- Membuat `userCopy` sebagai salinan dari `user` untuk dimodifikasi.
- Iterasi melalui kolom-kolom dalam baris dan memproses kondisi:
  - Jika iterasi mencapai indeks kolom yang digunakan sebagai kondisi WHERE (`index`), dan bukan iterasi pertama (`datake != 0`), serta nilai pada kolom yang digunakan sebagai kondisi WHERE (`user.data[i]`) sama dengan nilai kondisi (`where`), maka baris tersebut tidak akan disalin ke dalam file sementara (`found = 1`).
  - Jika tidak memenuhi kondisi, nilai kolom disalin dari `user`.
- Jika `found` tidak sama dengan 1, artinya kondisi WHERE tidak terpenuhi, maka baris tersebut disalin ke dalam file sementara (`fp1`).
- Meningkatkan `datake` untuk menandai iterasi ke berapa.
- Menutup file asli (`fp`) dan file sementara (`fp1`).
- Menghapus file asli (`remove(table)`) dan mengganti dengan file sementara (`rename("temp", table)`).
- Fungsi mengembalikan nilai 0.

#### Output

![where](gambar/where.png)

# E. Logging

Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.

| Format                                          | Contoh                                        |
| ----------------------------------------------- | --------------------------------------------- |
| timestamp(yyyy-mm-dd hh:mm:ss):username:command | 2021-05-19 02:05:15:khonsu:SELECT FROM table1 |

## Kode

### client.c

```bash
void writelog(char *perintah, char *nama)
{
	time_t rawtime;
	struct tm *timeinfo;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];

	FILE *file;
	char lokasi[10000];

	snprintf(lokasi, sizeof lokasi, "../database/databases/log%s.log", nama);
	file = fopen(lokasi, "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

```

### database.c

```bash
else if (strcmp(perintah[0], "log") == 0)
	{
	    writelog(perintah[1], perintah[2]);
		char peringatan[] = "\n";
		send(newSocket, peringatan, strlen(peringatan), 0);
		bzero(buffer, sizeof(buffer));
	}

```

```bash
void writelog(char *perintah, char *nama)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah);
    fputs(infoWriteLog, file);
	fclose(file);
	return;
}

```

## Penjelasan kode

### client.c

- `perintah`: String yang berisi perintah atau kejadian yang akan dicatat dalam log.
- `nama`: Nama pengguna atau entitas yang terkait dengan log tersebut.
- `time_t rawtime`: Representasi waktu kalender sebagai nilai bulat.
- `struct tm *timeinfo`: Struktur waktu yang menyimpan informasi waktu terpecah-pecah seperti tahun, bulan, hari, dll.
- `char infoWriteLog[1000]`: Array karakter untuk menyimpan informasi yang akan ditulis ke dalam file log.
- `char lokasi[10000]`: Array karakter untuk menyimpan path lengkap ke file log.
- `snprintf(lokasi, sizeof lokasi, "../database/databases/log%s.log", nama)`: Membuat path ke file log dengan format "../database/databases/log[NAMA].log", di mana `[NAMA]` adalah parameter `nama` yang diterima oleh fungsi.
- `time(&rawtime)`: Mendapatkan waktu saat ini dalam format waktu kalender.
- `timeinfo = localtime(&rawtime)`: Mengkonversi waktu kalender ke waktu lokal dan menyimpannya dalam struktur `timeinfo`.
- `FILE *file`: File pointer untuk membuka file log.
- `file = fopen(lokasi, "ab")`: Membuka file log untuk ditambahkan (`"ab"` artinya append binary mode).
- `sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, perintah)`: Mengisi array karakter `infoWriteLog` dengan informasi log yang akan ditulis. Format waktu adalah "YYYY-MM-DD HH:MM:SS".
- `fputs(infoWriteLog, file)`: Menulis informasi log ke dalam file menggunakan fungsi `fputs`.
- `fclose(file)`: Menutup file log setelah penulisan selesai.
- Fungsi ini tidak mengembalikan nilai (`void`).

### database.c

1. **Bagian Pemanggilan Fungsi `writelog`**:

   - Jika perintah yang diterima adalah "log", maka fungsi `writelog` dipanggil dengan parameter `perintah[1]` sebagai perintah dan `perintah[2]` sebagai nama.
   - Setelah itu, dikirimkan pesan newline (`"\n"`) melalui soket dengan menggunakan `send` untuk memberi tahu bahwa log telah ditulis.
   - `bzero(buffer, sizeof(buffer))` digunakan untuk mengosongkan buffer setelah menangani perintah log.

2. **Fungsi `writelog`**:
   - Fungsi ini menulis log ke dalam file dengan nama "logUser.log" dalam format tertentu.
   - Menggunakan fungsi `sprintf` untuk memformat string log dengan tanggal, waktu, nama, dan perintah.
   - File "logUser.log" dibuka dalam mode "ab" (append binary) untuk menambahkan log ke akhir file.
   - Log ditulis menggunakan `fputs` ke dalam file, dan kemudian file ditutup dengan `fclose`.

Keseluruhan, potongan kode ini mengimplementasikan pencatatan log saat menerima perintah "log" dengan menuliskan informasi waktu, nama, dan perintah ke dalam file "logUser.log".

## Output

![log](gambar/log.png)

# F. Reliability

- Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel.
- Program dump database dijalankan tiap jam untuk semua database dan log, lalu di zip sesuai timestamp, lalu log dikosongkan kembali.

| Format                                                                | Contoh                                                             |
| --------------------------------------------------------------------- | ------------------------------------------------------------------ |
| ./[program_dump_database] -u [username] -p [password] [nama_database] | ./databasedump -u khonsu -p khonsu123 database1 > database1.backup |

## Kode

```bash
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct allowed {
    char name[10000];
    char password[10000];
};

int cekAllowed(char *username, char *password) {
    FILE *fp;
    struct allowed user;
    int found = 0;
    fp = fopen("../database/databases/user.dat", "rb");
    if (fp == NULL) {
        printf("Failed to open user.dat file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp) == 1) {
        if (strcmp(user.name, username) == 0 && strcmp(user.password, password) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("You're Not Allowed\n");
        return 0;
    } else {
        return 1;
    }
}

void removeTimestampAndUsername(char *str) {
    char *pos = strchr(str, ':');
    if (pos != NULL) {
        char *end = strchr(pos + 1, ':');
        if (end != NULL) {
            memmove(str, end + 1, strlen(end + 1) + 1);
            // Menghapus karakter ':' sebelum perintah/command
            for (int i = 0; i < strlen(str); i++) {
                if (str[i] == ':') {
                    memmove(&str[i], &str[i + 1], strlen(str) - i);
                    break;
                }
            }
        }
    }
}

void removeLeadingSpaces(char *str) {
    int i = 0, j = 0;
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

int main(int argc, char *argv[]) {
    int allowed = 0;
    if (geteuid() == 0) {
        allowed = 1;
    } else {
        allowed = cekAllowed(argv[2], argv[4]);
    }
    if (allowed == 0) {
        return 0;
    }

    FILE *fp;
    char lokasi[10000];
    snprintf(lokasi, sizeof(lokasi), "../database/databases/log%s.log", argv[2]);
    fp = fopen(lokasi, "r");
    if (fp == NULL) {
        printf("Failed to open log file.\n");
        return 0;
    }
    char buffer[20000];
    while (fgets(buffer, sizeof(buffer), fp)) {
        if (strstr(buffer, "CREATE TABLE") || strstr(buffer, "INSERT INTO") || strstr(buffer, "SELECT")) {
            removeTimestampAndUsername(buffer);
            removeLeadingSpaces(buffer);
            printf("%s\n", buffer);
        }
    }
    fclose(fp);
    return 0;
}

```

## Penjelasan Kode

1. **Struktur `allowed`**:

   - Mendefinisikan struktur data `allowed` yang memiliki dua array karakter: `name` dan `password`. Struktur ini kemungkinan digunakan untuk menyimpan informasi pengguna yang diizinkan.

2. **Fungsi `cekAllowed`**:

   - Menerima username dan password sebagai argumen.
   - Membuka file "user.dat" dalam mode binary untuk membaca (rb).
   - Membaca data dari file dan memeriksa apakah ada entri dalam file yang cocok dengan username dan password yang diberikan.
   - Jika cocok, mengembalikan 1 (diizinkan), jika tidak cocok atau terjadi kesalahan, mengembalikan 0.
   - Fungsi ini digunakan untuk memverifikasi apakah pengguna memiliki izin untuk mengakses database.

3. **Fungsi `removeTimestampAndUsername`**:

   - Menerima string sebagai argumen.
   - Menghapus timestamp dan username dari string tersebut dengan memindahkan sisa string setelah dua titik pertama.
   - Menghapus karakter ":" sebelum perintah atau command.
   - Fungsi ini tampaknya digunakan untuk membersihkan log dari timestamp dan username sehingga hanya command yang terbaca.

4. **Fungsi `removeLeadingSpaces`**:

   - Menerima string sebagai argumen.
   - Menghapus spasi awal dari string tersebut dengan memindahkan karakter-karakter tanpa spasi ke posisi awal string.
   - Fungsi ini digunakan untuk membersihkan command dari spasi awal.

5. **Fungsi `main`**:
   - Memeriksa izin pengguna menggunakan fungsi `geteuid()` dan `cekAllowed`. Jika tidak diizinkan, program keluar.
   - Membuka file log dengan nama file yang terbentuk dari username (`log%s.log`).
   - Membaca baris-baris dari file log.
   - Jika baris tersebut mengandung "CREATE TABLE", "INSERT INTO", atau "SELECT", maka menghapus timestamp, username, dan spasi awal menggunakan fungsi `removeTimestampAndUsername` dan `removeLeadingSpaces`.
   - Mencetak hasilnya ke layar.
   - Program kemudian menutup file dan mengakhiri eksekusi.

# G. Tambahan

Kita bisa memasukkan command lewat file dengan redirection di program client.

| Format                                                                                 | Contoh                                                                |
| -------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| ./[program_client_database] -u [username] -p [password] -d [database] < [file_command] | ./client_databaseku -u john -p john123 -d database1 < database.backup |

# H. Error Handling

Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client

## Output

![error](gambar/error.png)
