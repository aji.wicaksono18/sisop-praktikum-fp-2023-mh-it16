#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PORT 4443
#define FILE_PATH "home/baebluu/praktikum/sisop/fp/database/databases"

struct UserData {
    char username[1000];
    char password[1000];
};

int checkPermission(char *username, char *password) {
    FILE *filePointer;
    struct UserData user;
    int permissionGranted = 0;

    filePointer = fopen(FILE_PATH, "rb");

    if (filePointer == NULL) {
        perror("Failed to access the file");
        return 0;
    }

    while (fread(&user, sizeof(user), 1, filePointer) == 1) {
        if (strcmp(user.username, username) == 0 && strcmp(user.password, password) == 0) {
            permissionGranted = 1;
            break;
        }
    }

    fclose(filePointer);

    if (permissionGranted == 0) {
        printf("Access Denied\n");
        return 0;
    } else {
        return 1;
    }
}

void processLog(char *username) {
    FILE *filePointer;
    char location[10000];

    snprintf(location, sizeof(location), "home/baebluu/praktikum/sisop/fp/database/databases/logs.log", username);
    filePointer = fopen(location, "r");

    if (filePointer == NULL) {
        perror("Failed to open the log file");
        return;
    }

    char buffer[20000];

    while (fgets(buffer, sizeof(buffer), filePointer)) {
        if (strstr(buffer, "CREATE TABLE") || strstr(buffer, "INSERT INTO") || strstr(buffer, "SELECT")) {
            stripTimestampAndUsername(buffer);
            removeLeadingSpaces(buffer);
            printf("%s\n", buffer);
        }
    }

    fclose(filePointer);
}

int main(int argc, char *argv[]) {
    int permission = (geteuid() == 0) ? 1 : checkPermission(argv[2], argv[4]);

    if (permission == 0) {
        return 0;
    }

    processLog(argv[2]);

    return 0;
}
